using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacementManager : MonoBehaviour
{
    public int width, height;
    //public int cellWidth, cellHeight;
    Grid placementGrid;
    //Grid[,] placementCells;

    Dictionary<Vector3Int, StructureModel> temporaryRoadObjects = new Dictionary<Vector3Int, StructureModel>();
    Dictionary<Vector3Int, StructureModel> structureDictionary = new Dictionary<Vector3Int, StructureModel>();

    void Start()
    {
        placementGrid = new Grid(width, height);
        //placementCells = new Grid[cellWidth, cellHeight];
    }

    internal bool CheckIfPositionInBound(Vector3Int position)
    {
        if(position.x >= 0 && position.x < width && position.z >= 0 && position.z < height) // && position.x < cellWidth && position.z < cellHeight)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    internal bool CheckIfPositionIsFree(Vector3Int position)
    {
        return CheckIfPositionIsOfType(position, CellType.Empty);
    }

    bool CheckIfPositionIsOfType(Vector3Int position, CellType empty)
    {
        return placementGrid[position.x, position.z] == empty;
    }

    internal void PlaceTemporaryStructure(Vector3Int position, GameObject roadPrefab, CellType road)
    {
        placementGrid[position.x, position.z] = road;
        StructureModel structure = CreateNewStructureModel(position, roadPrefab, road);
        temporaryRoadObjects.Add(position, structure);
    }    

    StructureModel CreateNewStructureModel(Vector3Int position, GameObject roadPrefab, CellType road)
    {
        GameObject structure = new GameObject(road.ToString());
        structure.transform.SetParent(transform);
        structure.transform.localPosition = position;
        var structureModel = structure.AddComponent<StructureModel>();
        structureModel.CreateModel(roadPrefab);

        return structureModel;
    }

    public void ModifyStructureModel(Vector3Int position, GameObject newRoad, Quaternion rotation)
    {
        if (temporaryRoadObjects.ContainsKey(position))
        {
            temporaryRoadObjects[position].SwapModel(newRoad, rotation);
        }
        else if (structureDictionary.ContainsKey(position))
        {
            structureDictionary[position].SwapModel(newRoad, rotation);
        }
    }

    internal CellType[] GetNeighbourTypesFor(Vector3Int position)
    {
        var neighbours = placementGrid.GetAllAdjacentCellTypes(position.x, position.z);
        return neighbours;
    }

    internal List<Vector3Int> GetNeighbourTypesFor(Vector3Int temporaryPosition, CellType road)
    {
        var neighbourVertices = placementGrid.GetAdjacentCellsOfType(temporaryPosition.x, temporaryPosition.z, road);
        List<Vector3Int> neighbours = new List<Vector3Int>();

        foreach (var point in neighbourVertices)
        {
            neighbours.Add(new Vector3Int(point.X, 0, point.Y));
        }
        return neighbours;
    }
    internal List<Vector3Int> GetPathBetween(Vector3Int startPosition, Vector3Int endPosition)
    {
        var resultPath = GridSearch.AStarSearch(placementGrid, new Point(startPosition.x, startPosition.z), new Point(endPosition.x, endPosition.z));
        List<Vector3Int> path = new List<Vector3Int>();

        foreach (Point point in resultPath)
        {
            path.Add(new Vector3Int(point.X, 0, point.Y));
        }
        return path;
    }

    internal void RemoveAllTemporaryStructures()
    {
        foreach (var structure in temporaryRoadObjects.Values)
        {
            var position = Vector3Int.RoundToInt(structure.transform.position);
            placementGrid[position.x, position.z] = CellType.Empty;
            Destroy(structure.gameObject);
        }

        temporaryRoadObjects.Clear();
    }

    internal void AddTemporaryStructuresToStructureDictionary()
    {
        foreach (var structure in temporaryRoadObjects)
        {
            structureDictionary.Add(structure.Key, structure.Value);
        }

        temporaryRoadObjects.Clear();
    }
}
