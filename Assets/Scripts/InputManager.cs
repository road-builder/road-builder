using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public Action<Vector3Int> OnMouseClick, OnMouseHold;
    public Action OnMouseUp;

    public LayerMask groundMask;

    [SerializeField] Camera mainCamera;

    void Update()
    {
        CheckClickDownEvent();
        CheckClickUpEvent();
        CheckClickHoldEvent();
    }

    Vector3Int? RayCastGround()
    {
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, groundMask))
        {
            Vector3Int positionInt = Vector3Int.RoundToInt(hit.point);

            return positionInt;
        }
        else
        {
            return null;
        }
    }

    private void CheckClickHoldEvent()
    {
        if (Input.GetMouseButton(0))
        {
            var position = RayCastGround();

            if(position != null)
            {
                OnMouseHold?.Invoke(position.Value);
            }
        }
    }

    private void CheckClickDownEvent()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var position = RayCastGround();

            OnMouseClick?.Invoke(position.Value);
        }
    }

    private void CheckClickUpEvent()
    {
        if (Input.GetMouseButtonUp(0))
        {
            OnMouseUp?.Invoke();
        }
    }
}
