using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadFixer : MonoBehaviour
{
    public GameObject roadStraight, roadCorner, roadTShaped, roadXShaped;

    public void FixRoadAtPosition(PlacementManager placementManager, Vector3Int temporaryPosition)
    {
        // [ left [0], up [1], right [2], down [3] ]
        var result = placementManager.GetNeighbourTypesFor(temporaryPosition);
        int roadCount = 0;
        roadCount = result.Where(x => x == CellType.Road).Count();

        if (roadCount == 0)
        {
            
        }
        else if (roadCount == 1)
        {
            if (CreateStraightRoad(placementManager, result, temporaryPosition))
            {
                return;
            }
        }
        else if(roadCount == 2)
        {
            if (CreateStraightRoad(placementManager, result, temporaryPosition))
            {
                return;
            }
            else
            {
                CreateCorner(placementManager, result, temporaryPosition);
            }
        }
        else if(roadCount == 3)
        {
            CreateRoadTShaped(placementManager, result, temporaryPosition);
        }
        else
        {
            CreateRoadXShaped(placementManager, result, temporaryPosition);
        }
    }

    private void CreateRoadXShaped(PlacementManager placementManager, CellType[] result, Vector3Int temporaryPosition)
    {
        placementManager.ModifyStructureModel(temporaryPosition, roadXShaped, Quaternion.identity);
    }

    // [ left [0], up [1], right [2], down [3] ]
    private void CreateRoadTShaped(PlacementManager placementManager, CellType[] result, Vector3Int temporaryPosition)
    {
        

        if(result[0] == CellType.Road && result[1] == CellType.Road && result[2] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadTShaped, Quaternion.identity);
        }
        else if(result[1] == CellType.Road && result[2] == CellType.Road && result[3] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadTShaped, Quaternion.Euler(0, 90, 0));
        }
        else if(result[2] == CellType.Road && result[3] == CellType.Road && result[0] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadTShaped, Quaternion.Euler(0, 180, 0));
        }
        else if(result[3] == CellType.Road && result[0] == CellType.Road && result[1] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadTShaped, Quaternion.Euler(0, 270, 0));
        }
    }

    // [ left [0], up [1], right [2], down [3] ]
    private void CreateCorner(PlacementManager placementManager, CellType[] result, Vector3Int temporaryPosition)
    {
        if (result[0] == CellType.Road && result[1] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadCorner, Quaternion.identity);
        }
        else if(result[1] == CellType.Road && result[2] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadCorner, Quaternion.Euler(0, 90, 0));
        }
        else if(result[2] == CellType.Road && result[3] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadCorner, Quaternion.Euler(0, 180, 0));
        }
        else if(result[3] == CellType.Road && result[0] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadCorner, Quaternion.Euler(0, 270, 0));
        }
    }

    // [ left [0], up [1], right [2], down [3] ]
    private bool CreateStraightRoad(PlacementManager placementManager, CellType[] result, Vector3Int temporaryPosition)
    {
        if (result[1] == CellType.Road && result[3] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadStraight, Quaternion.identity);
            return true;
        }
        else if(result[0] == CellType.Road && result[2] == CellType.Road)
        {
            placementManager.ModifyStructureModel(temporaryPosition, roadStraight, Quaternion.Euler(0, 90, 0));
            return true;
        }
        else
        {
            return false;
        }
    }
}
