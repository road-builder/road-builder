# Road Builder for Cyber Games S. A. Poland (Official page https://cgames.pl)

**This project is a test task done for Cyber Games**

*Controls*

The roads can be built in two ways:
- Click once on LMB to build a single block of a road;
- Hold LMB and drag to the desired place and then release to build a longer road.

*Additional info*

The "hold LMB to build a road system" is done using A* algorythm to find a path. Source - https://www.redblobgames.com/pathfinding/grids/graphs.html.
